package pl.androidwpraktyce.szkoleniew3_bazydanych.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by dkrezel on 2015-05-10.
 */
@DatabaseTable(tableName = "grupa")
public class Grupa {

	@DatabaseField(id = true, generatedId = true, columnName = "id")
	private int mID;

	// Unikalne pole, niepuste (NOT NULL)
	@DatabaseField(columnName = "nazwa", unique = true, canBeNull = false)
	private String mNazwa;

	@ForeignCollectionField
	private List<Grupa2Kontakt> mKontakty;

	public Grupa() {

	}

	public Grupa(String nNazwa) {
		this.mNazwa = mNazwa;
	}

	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public String getNazwa() {
		return mNazwa;
	}

	public void setNazwa(String mNazwa) {
		this.mNazwa = mNazwa;
	}

	public List<Grupa2Kontakt> getKontakty() {
		return mKontakty;
	}

	public void setKontakty(List<Grupa2Kontakt> mKontakty) {
		this.mKontakty = mKontakty;
	}
}
