package pl.androidwpraktyce.szkoleniew3_bazydanych.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dkrezel on 2015-05-10.
 */

@DatabaseTable(tableName = "telefon")
public class Telefon {

	@DatabaseField(id = true, generatedId = true, columnName = "id")
	private int mID;

	@DatabaseField(columnName = "rodzaj")
	private String mRodzaj;

	@DatabaseField(columnName = "numer_telefonu")
	private String mNumerTelefonu;

	@DatabaseField(foreign = true)		// foreign - klucz obcy w bazie danych
	private Kontakt mKontakt;		// Określenie relacji z tabelą Kontakt
	// Jeden obiekt telefonu ma związek z jednym obiektem typu kontakt;

	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public String getRodzaj() {
		return mRodzaj;
	}

	public void setRodzaj(String mRodzaj) {
		this.mRodzaj = mRodzaj;
	}

	public String getNumerTelefonu() {
		return mNumerTelefonu;
	}

	public void setNumerTelefonu(String mNumerTelefonu) {
		this.mNumerTelefonu = mNumerTelefonu;
	}

	public Kontakt getKontakt() {
		return mKontakt;
	}

	public void setKontakt(Kontakt mKontakt) {
		this.mKontakt = mKontakt;
	}
}
