package pl.androidwpraktyce.szkoleniew3_bazydanych.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by dkrezel on 2015-05-10.
 */
@DatabaseTable(tableName = "kontakt")
public class Kontakt {

	@DatabaseField(id = true, generatedId = true, columnName = "id")
	private int mID;

	@ForeignCollectionField(eager = true)
	private List<Telefon> mNumery;

	@DatabaseField(columnName = "imie")
	private String mImie;

	@DatabaseField(columnName = "nazwisko")
	private String mNazwisko;

	@DatabaseField(columnName = "wiek")
	private Integer mWiek;

	@ForeignCollectionField
	private List<Grupa2Kontakt> mGrupy;

	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public List<Telefon> getNumery() {
		return mNumery;
	}

	public void setNumery(List<Telefon> mNumery) {
		this.mNumery = mNumery;
	}

	public String getImie() {
		return mImie;
	}

	public void setImie(String mImie) {
		this.mImie = mImie;
	}

	public String getNazwisko() {
		return mNazwisko;
	}

	public void setNazwisko(String mNazwisko) {
		this.mNazwisko = mNazwisko;
	}

	public Integer getWiek() {
		return mWiek;
	}

	public void setWiek(Integer mWiek) {
		this.mWiek = mWiek;
	}

	public List<Grupa2Kontakt> getGrupy() {
		return mGrupy;
	}

	public void setGrupy(List<Grupa2Kontakt> mGrupy) {
		this.mGrupy = mGrupy;
	}
}
