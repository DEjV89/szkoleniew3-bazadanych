package pl.androidwpraktyce.szkoleniew3_bazydanych.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dkrezel on 2015-05-10.
 */
@DatabaseTable(tableName = "grupa2Kontakt")
public class Grupa2Kontakt {

	@DatabaseField(id = true, generatedId = true, columnName = "id")
	private int mID;

	@DatabaseField(foreign = true)
	private Kontakt mKontakt;

	@DatabaseField(foreign = true)
	private Grupa mGrupa;


	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public Kontakt getKontakt() {
		return mKontakt;
	}

	public void setKontakt(Kontakt mKontakt) {
		this.mKontakt = mKontakt;
	}

	public Grupa getGrupa() {
		return mGrupa;
	}

	public void setGrupa(Grupa mGrupa) {
		this.mGrupa = mGrupa;
	}
}
