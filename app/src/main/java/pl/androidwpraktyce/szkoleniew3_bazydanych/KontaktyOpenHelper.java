package pl.androidwpraktyce.szkoleniew3_bazydanych;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import pl.androidwpraktyce.szkoleniew3_bazydanych.model.Grupa;
import pl.androidwpraktyce.szkoleniew3_bazydanych.model.Grupa2Kontakt;
import pl.androidwpraktyce.szkoleniew3_bazydanych.model.Kontakt;
import pl.androidwpraktyce.szkoleniew3_bazydanych.model.Telefon;

/**
 * Created by dkrezel on 2015-05-10.
 */
public class KontaktyOpenHelper extends OrmLiteSqliteOpenHelper {

	private static final int DB_VERSION = 1;
	private static final String DB_NAME = "kontakty.db";

	public KontaktyOpenHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	// Jest wywoływana kiedy baza danych nie istnieje
	// Tutaj tworzymy strukturę bazy danych, ewentualnie dodajemy dane początkowe
	@Override
	public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
		try {
			// Tworzymy struktury tabeli w bazie danych
			TableUtils.createTable(connectionSource, Grupa.class);
			TableUtils.createTable(connectionSource, Kontakt.class);
			TableUtils.createTable(connectionSource, Telefon.class);
			TableUtils.createTable(connectionSource, Grupa2Kontakt.class);

			// Obiekt dostępu do danych w tabeli Grupa
			Dao<Grupa, Integer> mGrupaDAO = DaoManager.createDao(connectionSource, Grupa.class);
			mGrupaDAO.create(new Grupa("Rodzina"));
			mGrupaDAO.create(new Grupa("Znajomi"));
			mGrupaDAO.create(new Grupa("Partnerzy biznesowi"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Implementacja zmiany struktury z wersji na wersję
	@Override
	public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

	}
}
